**DESCRIPTION**  
The purpose of this project was to reproduce Dirk's 'UNCID' plot that he made using Excel, but by using *ggplot*. The end goal was to produce the same bubble plot with the 3 continuous variables and 1 categorical variable into a single 2D plot. The packages used were 'readxl', 'dplyr' and 'ggplot2'. The data and reference figure were provided by Dirk, which are all accessible on the group Confluence page.

**USAGE AND INSTALLATION**  
The only script in this project has only a single function, which is to plot the data points in the dataset as a bubble plot.

**DATA**  
The data generated contained a sizeable number of variables, but only 4 of which are necessary. The variables that we plot are: 'CtValue(Melissa)', 'MappedReadsCovidHuman', 'x100Percent', and 'x100bin'. Note that 'x100bin' is not part of the data and is the categorical values that need to be created using 'dplyr'.  

**PLOT**  
The 'ggplot2' package was used to generate the final plot. The plot was saved directly from the console.  
The following specifications were required for the final result:

* Aspect ratio at 1:1
* Include a legend
* Remove y-minor axis
* Transparency to help with overplotting

**PACKAGES**  
The 'readxl' package was used for importing the data from an Excel spreadsheet. The 'dplyr' package was used for general data wrangling. The 'ggplot2' package was used to generate the figure.

**CONTRIBUTORS AND CONTRIBUTIONS**   

Justin Landis (justin_landis@med.unc.edu)
Dr. Dirk Dittmer (dirk_dittmer@med.unc.edu)

**GUIDES AND RESOURCES**   
Grammar of Graphics with ggplot2: `https://ggplot2.tidyverse.org/`  
Function reference page (ggplot2): `https://ggplot2.tidyverse.org/reference/'`  

